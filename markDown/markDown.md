# Introduction au MarkDown.

## Les bases du MD :

***

`'#' Gros titre = h1`

`'##' 2eme titres = h2`

`'###' etc = h3 ... hx`


## Le visuel :

***

`'***' = bar en bas = <hr/->`


'` `' = petit encadrement

`'```' = gros encadrement + préciser le language (CSS, javascript, HTML...)`

***

`**text** = gras`


`_text_ = italic`

***


`> = en tête paragraphe`

`>x = x en tête`

***


`[Nom du lien](URL du lien) = lien`

***

```
<ul> ou <ol>
    <li> text n1</li>
    <li> text n2 </li>
</ul> </ol>
```

***

```
![Nom de l'image](url de l'image)  
           /  / Ou /  /
<img src="url de l'image" alt="= Nom de l'image" />
```
