#  CV de _Lucas TOUSVERTS_ 


## _`Mes Informations`_

***
```
                                             Lucas TOUSVERTS

                                                 18 Ans
                   
                                       13 Rue de l'Union NANTERRE

                                         TEL : +33 # ## ## ## ##

                                        lucas.tousverts@gmail.com
```
***

## _`Formations`_
***
**Lycée Newton CLICHY** - _2019_

- BAC STI2D Mention AB Option SIN

***

**BAFA Général CLICHY** - _2019_

- Obtention du BAFA général

***

**SST CLICHY** - _2019_

- Certification Sauveteur secouriste obtenue 
***

## _`Expériences Professionnelles`_

***
**Stage de découverte - L'Oréal CLICHY** - _2015_
- Observation de produits,Marketing, Design ...
***
## _`Compétences`_
***
- Connaissance en électronique
  et réseau.
  
- Utilisation d'outils numérique

- Abilité à répondre correctement à un cahier des charges

***
## _`Langues`_

***
- Anglais : Langage Courant

- Espagnol : Notions

- Allemand : Notions
***

## _`Loisirs`_

***
- Multimédia

- Jeux vidéos

- Cinéma 

- Musique 

- Veille Technologique 
***


