# BOOTSTRAPS

## Intro

1. Installer node.js si il s'agit d'un nouvel ordi (next next ...)

2. Dans terminal, écrire npm init =

3. Package name: (nom du dossier)

4. Puis spam space = package dans le projet

5. Ecrire `npm install bootstrap` **(Attention à l'écriture)**

> nouveau dossier 'node_modules'

- creer Html ... titre ... body ... h1 ...

- puis <link rel="stylesheet" href="...jusqu'a bootstraps.css
```
 <div class="container">
    <div class="col">1</div>
    <div class="col">2</div>
    <div class="col">3</div>
</div>
```

feuille css si envie de modif la position 

> pas recommender utiliser des offset-1 au lieu que margin(left,right)

## 1er Partie 

### Diff entre container et container_fluid.

- Container va ce centrer sur la page

- Alors que Container_fluid sur tout la page (ou taille si renseigner)

### Quelque base :

- row = ligne = horizontal

- col = colone = vertical

**Très important d'avoir toujours un col et un row**

- Une page "container" est diviser en 12 colone.

Donc si col = 6 alors il prendra la moitié

Si col 4 alors 1/3 etc

- offset permet de replacer directer des colones "vide" à gauche

De base les colones vides vont vers **la droite**

la valeur indiquer après le offset sera le nombre de colone !

- toujours finir un row par un col (row, col,row,col) et pas l'inverse.

_**[ ! (Ne Jamais Modifier Le Contenu CSS De Bootstrap ) ! ]**_