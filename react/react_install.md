# Introduction à react 

## React, c'est quoi ?

React est une bibliothèque JavaScript libre développée par Facebook depuis 2013.

Le but principal de cette bibliothèque est de faciliter la création d'application web en SPA (Single Page Application)
via la création de composants et générant une page HTML à chaque changement d'état.

## L'installation de React

1. Vérifier que NPM est bien installé sur la machine avec le terminal `npm -v`

2. Ajouter le CLI avec `npm i -g create-react-app` puis enter

3. Donner le nom de l'app avec `create-react-app [name]` enter puis patienter

4. Le projet a été créé avec un doc SRC

5. Lancer le projet avec `npm start` et [localhost:3000](localhost:3000)

