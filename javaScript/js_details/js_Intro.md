-__- -__- -__- -__- -__- -__- -__- -__-  **SOMMAIRE**  -__- -__- -__- -__- -__- -__- -__- -__-

- **Intro JS**

    - [_**Créer un index.js**_](#cration-dun-indexjs)
        
    - [_**Les Primitives**_](#les-valeurs-primitives)

        - [STRINGS](#strings)
        - [NUMBER](#number)
        - [BOULEAN](#boulean)
        - [UNDEFINED](#undefined)
        - [NULL](#null)
        - [NaN](#nan)
        
    - [_**Les Fonctions**_](#les-fonctions)
    
        - [LOG](#log)
        - [TABLE](#table)
        - [PROMPT](#prompt)
   
    - [_**Les Variables**_](#les-variables)
    
        - [~~VAR~~](#var)
        - [LET](#let)
        - [CONST](#const)  
    
-__- -__- -__- -__- -__- -__- -__- -__- -__- -__- -__- -__- -__- -__- -__- -__- -__- -__- -__- -__- -__- -__- 


# Introduction à JavaScript

## _Création d'un index.js_

1. Créer dans un projet, un `index.js` 

      ![comme ici](../../pictures/index.png)

    `Ce logo orange devrait apparaitre`

2. **Dans le head du HTML**, lier le fichier Js à HTML avec la balise :

    `<script src="index.js"></script>`

3. Dans le fichier js, vérifier via la commande : 
```
-_-_-_-_-_ console.log('Bonjour'); _-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-
              ↑     ↑     ↑      ↑
              |     |     |      |
           Destinati|on   |      | 
                    |     |      |
                Fonction  |   TOUJOURS FINIR UNE COMMANDE JS PAR ;
                          |
                       Message (entre single quotes ou double)
-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-
```
- Checker dans la console de la page HTML avec `F12` que le message est bien afficher.

    ![Bonjour](../../pictures/Bonjour.PNG)
    
    `Un message dans la console devrait apparaitre`

## _Les valeurs Primitives_

***
### _Strings_
- Les _**Strings**_ qui seront définit par des single quotes ( ou double ) :

     `'Bonjour'` / ou / `"Coucou"` : Ils seront écris en **vert**
     
***
### _Number_
- Les _**Number**_ qui seront directement défini à l'écriture :

    `42 ; 999 ; 46.8` : Ils seront écris en **bleu**
    
***
### _Boulean_
- Les _**Boulean**_ qui sont des valeurs **binaire** soit 1 ou 0, vrai ou faux etc :

   `True` et `False` : Ils seront écris en **orange**
   
***
### _Undefined_
- La valeur _**Undefined**_ qui est une valeurs **innexistante** :

    `undefined` : Il sera écris en **orange**
    
***
### _Null_
- La valeur _**Null**_ qui est une valeur existante mais **vide** :

    `null` : Il sera écris en **orange**
    
***
### _NaN_
- La valeur _**NaN**_ (Not a Number) qui va définir un résultat en texte comme correct :

    `NaN` : Il sera écris en **orange**
    
***

## _Les fonctions_

***
### _Log_
- La fonction _**Log**_ permet de laisser une trace de texte à une zone donnée

     _Par exemple_ = `console.log('Bonjour');` comme vue au dessus
   
***
### _Table_
- La fonction _**table**_ est pratiquement la même que log. En effet, elle permet d'afficher un tableaux crée avec [ARRAY](js_Array.md)

     _Par exemple_ = `console.table('Légumes','Fruits');` 
   
***
### _Prompt_
- La fonction _**Prompt**_ qui permet d'éfféctuer une demande à l'utilisateur

    _Par exemple_ = `prompt (message: "Bonjour, rentrez votre nom");`

***
## _Les variables_

_**Les variables permettent de stocker des info (Number,Strings) pour pouvoir les manipuler**_

Fonctionnement d'une variable : `variable [nom de la variable] = valeur de la variable;`

***
### _~~Var~~_
~~- La variable `var` n'est plus utilisé~~

***
### _Let_
- La variable `Let` sera modifiable en cas de réasignation.

    _Exemple_ : 
    
    ```
        let number1 = 10;
  
        number1 = 14
  
        La valeur final sera 14
   ```
### _Const_
- La varable `Const` sera constament la même et plus modifiable.

    _Exemple_ :
    
     ```
        const number2 = 8;
      
        number2 = 26
      
        La valeur final sera 8.
     ```
> Adapter les variables par rapport au projet.
