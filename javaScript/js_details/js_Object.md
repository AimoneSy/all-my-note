# _Les objets_

Les **objets** permettent de créer des boites sur lesquels on peut y stocker plusieur _variable et fonctions_.

- Exemple :

```       
const me = { <= début objet
    test1: 'Ne pas dupliquer les noms' ,
    test2: 'Ne pas dupliquer les noms' ,
    test3: 'Ne pas dupliquer les noms' ,
}; <= fin objet
```
