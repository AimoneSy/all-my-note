# Les Truthy et Falsy 

## _La diffèrence ?_

- Les `Truthy` sont des valeurs, des primitives et éléments qui renveront un `Truth`.

- Les `Falsy` sont des valeurs, des primitives et éléments qui renveront un `False`.

> On peut faire un test avec `if` dans la console, par exemple : `if (true) {console.log ('Test')}`

> Ou bien : `if (false) {console.log ('Test')}`

## _Les diffèrents Truthy et Falsy_

```javascript
======================== TRUTHY ======================== = ======================== FALSY ========================
                                                         |
- Boulean (True)                                         | - Boulean (False)
                                                         |
- Les numbers (1, 40, -10, 10000)                        | - 0
                                                         |                               
- Les strings remplis ('Test','Bonjour',' ')             | - Les strings vides ('')                                  
                                 /*Avec espace*/         |
- Les Array et object meme vide []                       | - X
                                                         |
- X                                                      | - Les primitives (null, undefined, NaN)
                                                         |
======================================================== = =======================================================
```

## _Inverse avec l'opérateur logique `!`_

- Ce caractère permet d'inverser le resultats d'une valeur, primitives ou éléments.

- Par exemple : `!false` est égal à `true`

- Par conséquent, d'après le tableau vue ci dessus, le `!` permet de passer un truthy vers un falsy et vice versa.

- On peut éventuellement en mettre 2, mais le resultat de fin sera égal à la forme de base.


