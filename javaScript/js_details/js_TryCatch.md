# Instruction Try & Catch 

## Son but ?

- Il permet d'éffectuer du code en protégeant l'intégralité du serveur et du code.

- Par exemple, si un `app.get` est déffectueux, il ne ferra pas sauter tout le serveur.  

- La partie Catch est un comme un `else`, si la partie `try` ne fonctionne pas, alors nous allons écrire ce qui se trouve à l'intérieur.

> Il est **PRIMORDIAL** dans un code désormais !

## Son utilisation ?

Dans la partie `try`, il faut y déposer le code à exécuter. Par exemple :

```javascript
try {
  notWorkingFunction();
}
/* catch (e) {
  console.error(error);
} */
```

> la fonction si dessus n'existe pas, ainsi, elle va créer une erreur.

Cette erreur va être gérer pas le `catch` qui va devoir être utiliser comme ceci :

```javascript
try {
  notWorkingFunction();
} catch (e) {
  console.error(e);
}
```

- Nous avons vue que le ``try`` ne fonctionner pas, désormais, le ``catch`` va renvoyer une erreur indiquer dans le `(e)`.

> On peut aussi renvoyer un code de status, [**_voici la liste_**](https://developer.mozilla.org/fr/docs/Web/HTTP/Status) des diffèrents code selon le message de sortie.

- Voici un très bon exemple à placer dans un catch :

```javascript
try {
  notWorkingFunction();
} catch (e) {
  return res.status(500).send({error: 'erreur serveur :' + e.message});
}
```