# Organiser le JavaScript

**Les fichiers Java Script doivent être bien organiser et propre**

**Mieux vaut un fichier bien _organiser qui ne marche pas que l'inverse !_**

**Faire un bon naming pour les variable, functions, etc ...**

## Distribution des fichiers

1. Créer plusieur fichier (comme dans le CSS) en fonction de leur rôle dans le **projet**

2. Par exemple un fichier : `user.data.js` pour y déposer tout les **info user**

3. Dans le fichier avec les data, créer un export pour la **variable choisis**

     >Exemple : `export const test(Oui)`

4. Puis dans le fichier qui reçoit : `import {test} from ./user.data;`
                            
## Naming des fichiers 

> Taches très basique mais extrèments importantes

- Renomer les fonctions/variables par rapport à leur but au sein du projet, fichier ...

> Toujours écrire en camelCase et en Anglais

- Définir la 1er partie de la fonction, par exemple : **find, replace, delete**

- Puis la 2eme qui sera l'objet cible, par exemple : **hostels, user, id**

- Et enfin la dernière (optionnel) qui peut être un des 2 exemples du haut

