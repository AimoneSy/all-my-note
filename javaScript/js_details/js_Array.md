-__- -__- -__- -__- -__- -__- -__- -__-  **SOMMAIRE**  -__- -__- -__- -__- -__- -__- -__- -__-

- [**ARRAY**](#lobjet-array)

    - _**Créer son tableau**_
        - -__- [Lenght](#accéder-à-un-élément-du-tableau) -__-
        
    - _**Les Opérateurs**_

        - -__- [PUSH](#ajouter-à-la-fin-du-tableau-push) -__-
        - -__- [UNSHIFT](#_ajouter-au-dbut-du-tableau-unshift) -__-
        - -__- [POP](#_supprimer-le-dernier-lment-du-tableau-pop) -__-
        - -__- [SHIFT](#shift-supprimer-le-premier-lment-du-tableau) -__-
        - -__- [IndexOf](#indexof-trouver-lindex-dun-lment-dans-le-tableau_) -__-
        - -__- [REDUCE](#reduce-accumuler-plusieur-valeur-dune-liste_) -__-
        - -__- [SPLICE](#splice-supprimer-un-lment-par-son-index_) -__-
        - -__- [SLICE](#slice-copier-un-tableau_) -__-
        - -__- [ForEach](#foreach-chercher-dans-un-tableau) -__-
        - -__- [MAP](#map-chercher-dans-un-tableau-et-le-dupliquer) -__-
        - -__- [FIND](#trouver-un-élément-dans-un-tableau-avec-une-condition-find) -__-
        - -__- [SORT](#trier-un-tableau-sort) -__-
        - -__- [FILTER](#filtrer-un-tableau-filter) -__-
        - -__- [EVERY](#tous-les-éléments-doivent-satisfaire-à-la-condition-true-every) -__-
        - -__- [SOME](#au-moin-un-élément-doit-satisfaire-à-la-condition-true-some) -__-
        
        - _**Cheat Sheet**_
            ![CheatSheet](../../pictures/CheatSheet.jpg)
       


-__- -__- -__- -__- -__- -__- -__- -__- -__- -__- -__- -__- -__- -__- -__- -__- -__- -__- -__- -__- -__- -__- 

# _L'objet Array_

**L'objet global `Array` est utilisé pour créer des tableaux.**

## _Création d'un tableau_

```javascript
const legumes = ['Salade','Ail','Poivron'];
```

>Définir la variable voulu : Dans ce cas il s'agit une variables const name = "legumes"

>Le signe égal pour que "legumes équivaut à salade, ail et poivron"

>Entre [crochet], nous avons 3 strings : 'Salade', 'Ail' et 'legumes' tous séparés d'une virgule

```javascript
console.table(legumes);
```

> console.table permet d'afficher la variable sous formes de tableau

 ![Résultat](../../pictures/screnjs.PNG)

***
### _Accéder à un élément du tableau_

```javascript
let first = legumes[0];
```

> 2 nouvelles variable sont crée pour retrouver les diffèrents index

> le 0 est celui en première position, donc ce sera la salade
>
```javascript
let last = legumes[legumes.length - 2];
```

> le 2 est celui en dernière position, donc ce sera le poivron
***

## _Les opérateurs_

###  _Ajouter à la fin du tableau_`[PUSH]` 

```javascript
let newVegetablesAtEnd = legumes.push('Artichaut');
```

> La nouvelle variable 'newVegetablesAtEnd' va push(ajouter) à la fin du array le nouvel élément `'Artichaut'`

![screen](../../pictures/screen2js.PNG)
***
###  _Ajouter au début du tableau_`[UNSHIFT]`

```javascript
let newVegetablesAtStart = legumes.unshift('Brocoli');
```

> La nouvelle variable 'newVegetablesAtStart' va unshift(ajouter) au début du array le nouvel élément `'Brocoli'` 

![Screen](../../pictures/screen3js.PNG)
***
###  _Supprimer le dernier élément du tableau_ `[POP]`

```javascript
let lastRemove = legumes.pop();
```

> Le dernier élément sera pop(supprimer le last) et stocker dans la variable lastRemove = `Artichaut`
***
###  _Supprimer le premier élément du tableau_ `[SHIFT]`

```javascript
let firstRemove = legumes.shift(); 
```

> Le premier élément sera shift(supprimer le first) et stocker dans la variable firstRemove = `Salade`

***
###  _Trouver l'index d'un élément dans le tableau_ `[IndexOF]`

```javascript
let pos = legumes.indexOf('Ail');
```

> IndexOf vas trouver l'index de l'élément indiquer entre (paranthèse)

> L'index de ail est 2 

![screen](../../pictures/screen4js.PNG)
***
###  _Accumuler plusieur valeur d'une liste_ `[REDUCE]`

```javascript
const legumes = ['Brocoli','Poivron','Artichaut','Salade','Ail'];

legumes.reduce((acc, curentValue) => acc = acc + curentValue, ' ');
```
> La valeur ' ' n'est pas obligatoire mais `fortement recommandé` il permet de déffinir la valeur en sortie !
***
###  _Supprimer un élément par son index_ `[SPLICE]`

```javascript
let removedItem = legumes.splice(start: 0, deleteCount: 1 ,);
```
***
> Le premier nombre entre crochet sera l'élement de départ choisi avec l'index  Ex: 2 pour 'Ail'

> Le deuxième nombre sera la quantité d'élément à supprimer. Si [2, 2] alors de l'index 2 il y aura 2 éléments suppr (y compris l'index)

> Un élément peut être ajouter après le `deleteCount`, il permet de placer une valeur au début du tableau

###  _Copier un tableau_ `[SLICE]`

```javascript
let copy = legumes.slice(); 
```

> Slice copie les éléments du tableau pour les intégrer sur une nouvelle variables

***

###  _Chercher dans un tableau et appliquer sur chaque éléments individuellement une condition_ `[ForEach]`

```javascript
const legumes = ['Brocoli','Poivron','Artichaut','Salade','Ail'];

legumes.forEach(element => console.log(element));
```
> Ici, forEach va aller chercher tous les élément qui compose 'légumes' et leur appliquer un `console.log`
***
###  _Chercher dans un tableau et le dupliquer avec/sans condition_ `[Map]`

```javascript
const legumes = ['Brocoli','Poivron','Artichaut','Salade','Ail'];

const addSAtEnd = legumes.map(legumes => legumes + 's');
```
> Ici, Map va rajouter à la fin de chaque légumes le caractère 's'

![Screen](../../pictures/screen5js.PNG)
***
###  _Trouver un élément dans un tableau avec une condition_ `[FIND]`

```javascript
const found = legumes.find(legumes => legumes.length >= 8);
```
>Ici, parmis la liste de tous les légumes. Find vas chercher l'éléments qui est supérieur ou égal à 8 caractères

>Il va ainsi renvoyer `'Artichaut'` qui est le seul mot supérieur ou égal à 8

**`[ ! 2 dangers ! ]`**

- **Find ne peut trouver qu'un seul élément à la fois (celui en 1er qui correspond aux conditions)**

- **Il ne peut pas être chain par d'autre opérateurs !**
***
###  _Trier un tableau_ `[SORT]`

```javascript
const legumes = ['Brocoli','Poivron','Artichaut','Salade','Ail'];

legumes.sort();
```
> Les éléments sont des **strings**, donc il seront classer dans l'ordre alphabétique 

> on aura donc = `['Ail','Artichaut','Brocoli','Poivron','Salade];`
***
###  _Filtrer un tableau_ `[FILTER]`

```javascript
const legumes = ['Brocoli','Poivron','Artichaut','Salade','Ail'];

const filteredVegetables = legumes.filter(legumes => legumes.length > 6);
```
> Les élément qui ont plus de 6 caractère seront ajouter à la constante 'filteredVegetables'

> **filteredVegetable =** `['Brocoli','Poivron','Artichaut']`
***

###  _TOUT les éléments doivent satisfaire à la condition = True_ `[EVERY]`

```javascript
const every = legumes.every(legumes => legumes.length >= 5);
```
> 'Artichaut','Salade','Poivron','Ail','Brocoli'

> 4 légumes sur 5 ont bien 5 caractères ou plus sauf 1 donc valeur de sortie = **false**
***

###  _Au MOINS un élément doit satisfaire à la condition = True_ `[SOME]`
```javascript
const some = legumes.find(legumes => legumes.length >= 8);
```
> 'Artichaut','Salade','Poivron','Ail','Brocoli'

> 1 légumes sur 5 à 8 caractères ou plus, valeur de sortie = **true**

***
