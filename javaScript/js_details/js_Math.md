# Math Java Script

## _Introduction_

- Le language **Java Script** est un mauvais outil pour réaliser des **Mathématiques**.

- Un exemple : 

```javascript
console.log(0.1 + 0.2);
``` 
> le résultat attendu est bien évidemment `0.3`, pourtant son résultat sera : ![Screen](../../pictures/screen9js.PNG)

`Il existe donc quelque outils pour y remédier et pour gérer les numbers`
***

## _Les Outils_

Certain de ces outils font parti de la librairie `Math`. Le number à affécter doit être entre **Parenthèse**.

> `console.log(Math.[Outil](valeurDuNumber)) `

### `[Round]` _Arrondir un nombre à virgule à l'unité_

```javascript
console.log(Math.round(10.49));
//Résultat attendu === 10

console.log(Math.round(10.5));
//Résultat attendu === 11
```

> Au dessus de **0.49** === **1** / En dessous de **0.49** === **0** / égal à **0.49** === **0**

***
### `[Flour]` _Supprimer après virgule_

```javascript
console.log(Math.floor(10.49));
//Résultat attendu === 10

console.log(Math.floor(10.5));
//Résultat attendu === 10
```

> Il retire la virgule peut importe la valeur du **number**

***
### `[ToFixed]` _Fixer une quantité de nombre après virgule_

```javascript
console.log(10.495565);
//Résultat attendu === 10.495565

console.log(10.495565.toFixed(3));
//Résultat attendu === 10.495
```

> `[Attention]` = Après avoir appliquer un .toFixed à une valeur. Celle-ci devient un string !

```javascript
console.log(10.495565.toFixed(3));
//Résultat attendu === 10.495

console.log(10.495565.toFixed(3) + 10);
//Résultat attendu === 20.495
```

> Le résultat attendu devrait être de 20.495. Négatif ! Car `10.495` est désormais un string donc le resulat sera = `10.49510`

***
### `[ParseInt]` _Définir un nombre sans virgule en number_

```javascript
console.log(10.toFixed(3) + 10);
//Innutile de faire un .toFixed sur un number sans virgules mais c'est pour l'exemple !
//Résultat attendu === 10 <= String + 10 = 1010

console.log(parseInt(10.toFixed(3) + 10.495));
//Résultat attendu === 10 <= number + 10 = 20.495
```

> `parseInt` est pour les numbers sans virgules

***
### `[ParseFloat]` _Définir un nombre avec virgule en number_

```javascript
console.log(10.495565.toFixed(3) + 10);
//Résultat attendu === 10.495 <= String + 10 = 10.49510

console.log(parseFloat(10.495565.toFixed(3) + 10));
//Résultat attendu === 10.495 <= number + 10 = 20.495
```

> `parseFloat` est pour les numbers avec virgules

***
### `[Random]` _Crée un nombre Pseudo-Aléatoire_

```javascript
console.log(Math.random());
```

- On obtient un nombre aléatoire sous cette forme ![Screen](../../pictures/screen10js.PNG)

- Il sera toujours diffèrents 

- Ces valeurs seront comprise entre `0.000000000000000000` et `1` mais jamais atteindre 1.000000000000000000

***

`[ NB ] :` Pourquoi **Pseudo**-Aléatoire ?
    
- Ca sera la **mémoire vive** de l'ordinateur et **l'horloge interne** du micro processeur qui determinera un nombre, il peut donc être anticiper ce qui n'est pas vraiment '**Aléatoire**'

***

#### Avoir un nombre aléatoire entre 2 valeurs

```javascript
function createRandomNumber(min, max) {
  return Math.floor(min + Math.random() * (max - min));
}

console.log(createRandomNumber(10, 15)); 
```

> Les valeurs seront comprise entre 10 et 15 mais n'atteindra pas 15. (Faire + 1 après (max - min) pour aller à 15 )
