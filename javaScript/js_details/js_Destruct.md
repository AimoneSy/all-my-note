# Destructuration dans Java Script

## A quoi sert la destructuration ?

La **Destructuration** permet de récupérer des variables dans un **Array** ou un **Object** et de leur donner un nom.

## Comment l'utiliser pour un _Array_ ?

### Modifier un élément

```javascript
const legumes = ['Poivron', 'Salade', 'Brocoli', 'Artichaut', 'Ail'];
        
        //Index =    0         1          2           3         4

console.log(legumes);

legumes[2] = 'Concombre';
//Brocoli sélectionner remplacer par Concombre

console.log(legumes);
```

>Le premier `console.log` ne contiendra pas **Concombre** car il a été placer avant la modification

![Screen](../../pictures/screen6js.PNG)

### Trouver un élément et l'intéger dans une variables _(**Mauvaise Méthode**)_

```javascript
const legumes = ['Poivron', 'Salade', 'Brocoli', 'Artichaut', 'Ail'];
        
        // Index =    0         1         2           3         4

const findBrocoli = legumes[2];
const findAil = legumes[4];

console.log(findBrocoli, findAil);
```

![Screen](../../pictures/screen7js.PNG)

### Trouver un élément et l'intéger dans une variables _(**Nouvelle Méthode**)_

```javascript
const legumes = ['Poivron', 'Salade', 'Brocoli', 'Artichaut', 'Ail'];
        
        // Index =    0         1         2           3         4

       const [linkToPoivron, linkToSalade, linkToBrocoli, linkToArtichaut, linkToAil] = legumes;
 
// chaque élément de la constante sera lier à des éléments de la const legumes (peu importe le nom ou primitives)

console.log(linkToArtichaut);
```

> Chaque élement est désormais inscrit dans des variable individuelle 

> Si (Par exp) il y a des espace à la place de 'linkToPoivron', rien ne sera égal a Poivron

## Comment l'utiliser pour un _Object_ ?

```javascript
const legumes = {
    firstVegetable: 'Poivron',
    secondVegetable: 'Ail',
    thirdVegetable: 'Artichaut',
    fourthVegetable: 'Salade',
    fifthVegetable: 'Brocoli'
};

const {firstVegetable, thirdVegetable} = legumes;

console.log(firstVegetable, thirdVegetable);
```

![Screen](../../pictures/screen8js.PNG)


