# La boucle For Java Script

**L'instruction `For` crée une boucle composée de trois expressions optionnelles séparées par des points-virgules et encadrées entre des parenthèses**

## Comment l'écrire ?

```javascript
for ([initialisation]; [condition]; [expression_finale]) {
   instruction
}
```

### Initialisation
```
Une expression ou une déclaration de variable.
Cette expression est évaluée une fois avant que la boucle démarre.
On utilise généralement une variable qui agit comme un compteur.

Cette expression peut éventuellement déclarer de nouvelles variables avec le mot-clé var ou let.
Les variables déclarées avec let sont locales à la boucle. 
Le résultat de l'expression n'est pas utilisé.
```
### Condition
```
Une expression qui est évaluée avant chaque itération de la boucle.
Si cette expression est vérifiée, l'instruction est exécutée.

Ce test est optionnel. S'il n'est pas présent, la condition sera toujours vérifiée.
Si l'expression n'est pas vérifiée (i.e. vaut false), l'exécution se poursuivra à la première expression qui suit la boucle for.
```
### Expression_finale
```
Une expression qui est évaluée à la fin de chaque itération.

Cela se produit avant l'évaluation de l'expression condition.
Cette expression est généralement utilisée pour mettre à jour ou incrémenter le compteur qu'est la variable d'initialisation.
```
### Instruction
```
Une instruction qui est exécutée tant que la condition de la boucle est vérifiée.

Afin d'exécuter plusieurs instructions au sein d'une telle boucle,
il faudra utiliser une instruction de bloc ({ ... }) qui regroupera ces différentes instructions.
```
> Voici un exemple : 

```javascript
for (let i = 0; i < 15; i = i + 1) {
  console.log('Test');
}
```
> Pour commencer, la valeur de départ de `i` sera de 0.

> Tant que `i` est inférieur à 15, il continuera les instruction.

> A chaque fin d'itération, la valeur `i` s'incrémente en + 1.

> Ici, la boucle `For` va console log Test.

- Ici, nous auront 15 fois écris 'Test' dans la console.

- La 1er et 3eme partie est optionnel.

- Si la condition n'atteint jamais la limite = Boucle infinie !
