# Quelque code en Js avec Array

**Voici quelque code vue en cours utilisant les Arrays qui sont très utilie dans le monde du code.**

**Les data se trouvent dans [Array Data](js_Array_Data.Hostels.md)**
## Les codes :

```javascript
hostels.forEach((hotel)=> {
  hotel.rooms.forEach((room)=> room.roomName = capi(room.roomName));
});

function capi(word) {
  if (typeof word === 'string') {
    return word.charAt(0).toUpperCase() + word.slice(1);
  } else {
    return ' ';
  }
}

console.log(hostels);
```

> Ici, nous allons chercher TOUTE les rooms de TOUT les hostels et assigner la fonction capi à tous les roomName.

> Donc, pour tous les hostels, chaque room aura une majuscule au premier caractère.

```javascript
hostels = hostels.sort( (x, y) => y.roomNumbers - x.roomNumbers);

const hostelName = hostels.map((hotel) => hotel.name );

console.table(hostelName);
```

> Ici, dans un premier temps, nous allons faire un rangement des hostels et placer celui qui à le plus de room en 1er.

> Puis nous allons crée un tableau uniquement avec les noms d'hostels.

```javascript
const allRooms = hostels
    .reduce((acc, hotel)=> [...acc, ...hotel.rooms], [])
    .filter((room)=> room.size >= 3)
    .sort((a, b)=> a.roomName < b.roomName ? -1 : 1);

console.log(allRooms);
```

> Désormais, nous allons prendre les rooms de chaque hostels avec le reduce pour les placer sur une const.

> Filtrer les rooms et garder seulement celle qui sont supérieur ou égal à 3 place.

> Puis faire un sort par roomName.
