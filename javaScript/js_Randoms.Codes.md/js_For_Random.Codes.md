# Quelque code en Js avec For

**Voici quelque code vue en cours utilisant la boucle For qui sont très utiles dans le monde du code.**

## Les codes :

```javascript
function betweenZeroAndNumber(numb) {
  for (let i = 0; i <= numb; i++) {
    console.log(i);
  }
}
betweenZeroAndNumber(10);
```

> Ici, nous allons tirer une valeur par exemple 10. Cette fonction permet d'afficher tout les valeur de 0 jusqu'a la valeur défini (10).

```javascript
function betweenTwoNumber(min, max) {
  for (let i = min; i <= max; i++) {
    console.log(i);
  }
}
betweenTwoNumber(20, 30);
```

> Un peu comme l'exo précedent, nous allons cette fois si tirer 2 valeurs. Cette fonction va écrire tout les valeur entre la valeur min et max.

```javascript
function lotoDraw(min, max) {
  for (let i = 0; i < 7; i++) {
    console.log(Math.floor(min + Math.random() * (max - min)));
  }
}
lotoDraw(1, 45);
```

> Cette fonction permet de faire un tirage de loto. Ici nous allons piocher 7 valeur comprise entre 1 et 45. (mais 45 n'apparaitra pas)

```javascript
const pairArray = [];

let int = 2;

for (let i = 0; i < 20; i++) {
  int = 2 + int;
  pairArray.push(int);
}
console.log(pairArray);
```

> Ici, nous allons créer un tableau composer de 20 valeurs composer uniquement de valeur pair. La première valeur sera égal à 4.

```javascript
 const firstName = [
   'Paul1',
   'Paul2',
   'Paul3',
   'Paul4',
   'Paul5',
   'Paul6',
   'Paul7',
 ];

 const lastName = [
   'DuPont1',
   'DuPont2',
   'DuPont3',
   'DuPont4',
   'DuPont5',
   'DuPont6',
   'DuPont7',
 ];

 function randomValues(min, max) {
   return Math.floor(min + Math.random() * (max - min));
 }

 function getValues(array) {
   return array[randomValues(0, array.length)];
 }

 function createUser(end) {
   const tab = [];

   for (let i = 0; i < end; i++) {
     tab.push({
       firstName: getValues(firstName),
       lastName: getValues(lastName),
       age: randomValues(1, 40),
     });
   }
   return tab;
 }

 console.log(createUser(100));
```
> Ici, le but est de générer un Array avec un firstName, un lastName et un age tous les 3 aléatoire. Nous voulons 100 utilisateurs

> randomValues va générer des valeurs avec un max et un min utile pour l'age tandis que getValues va piocher (avec randomValues) une valeur entre 0 la taille des const firstName et lastName.

> Tant que i est inférieur à end qui est égal à 100, il va continuer de push un firstName et un lastName tout deux random avec getValues et age avec randomValues.

```javascript
for (let i = 1; i <= 199; i++) {
  if (i % 3 ===0 && i % 5 === 0) {
    console.log('FizzBuzz');
  } else if (i % 3 === 0) {
    console.log('Fizz');
  } else if (i % 5 === 0) {
    console.log('Buzz');
  } else {
    console.log(i);
  }
}
```

> Ici nous allons console.log tout les valeurs de 1 à 199.

> Mais pour chaque valeur si le reste d'une division est égal à 3 et 5 ou 3 ou 5, il auront les attribut FizzBuzz , Fizz ou Buzz sinon, leur valeur de base sera afficher.

> Le % n'est pas la division mais le 'Modulo', il s'agit du reste d'une division donc par conséquent une valeur divisible par 3 aura un modulo à zéro .
