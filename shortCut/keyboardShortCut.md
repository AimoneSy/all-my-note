# Quelque raccourcis importants


## Raccourcis Windows

`windows + L` = Vérrouiller la session.

`windows + R` = Rechercher 


## Raccourcis clavier sur WS

`CTRL + D` = Dupliquer une ligne (ou sélection)

`CTRL + Y` = Delette une ligne (ou sélecion)

`CTRL + Z` = undo 

`CTRL + Maj + Z` = redo

`CTRL + Spacebar` = suggestion 

`Maj + flèches` = sélection.

`CTRL + -` = masquer la selection/div

`CTRL + A` = sélectioner **TOUTE** la page

`CTRL + F` = recherer sur WS un caractère/Fonction ...

`CTRL + Shift + /` = passer la séléction en commentaire

`CTRL + Shift + fleche haut/bas` = deplacer une selection 

`CTRL + Shift + A` = rajouter les fichiers séléctionnés sur Git 

`Shift + Alt + fleche haut/bas` = deplacer la ligne

`Shift + Suppr(en bas à droite du clavier)` = delete droite ou Suppr(au dessus des fleches)

`Shift + Fin` = Sélectionner du début du clic à la fin de la ligne

`Shift + Début` = Sélectionner du début du clic au début de la ligne

> **Au dessus des flèches (Fin et Debut)**

`Alt + Enter` = Importer un fichier, une fonction ...

`CTRL + Shift + Alt + J` = Modif tout les éléments identique et les modif tous !

`Alt + fleches droite ou gauche` = changement de fichier (bar du haut)

## Raccourcis écris sur WS

`div.bonjour` + `tabulation` = `<div class="bonjour"></div>`

`&nbsp` est un espace "insécable", très important !

## Raccourcis Clavier divers


### Sur un site ou/ page html

`CTRL + molette` = zoom / dezoom

`CTRL + Shift + C` = instant inspect + detail afficher

`CTRL + Shift + T` = ouvrir la dernière page fermée

`CTRL + F` = recherer sur une page un caractère/mot

`F12` = Ouvrir la console directement
