# Introduction à FireBase 

## FireBase ? C'est quoi ?

**FireBase est un outil d'hébergement gérer et proposer par _Google_ via serverLess**

**Il permet de gérer les authentifications, données et autre pour des pages HTML**

## Comment utiliser FireBase ?

1. Se rendre sur [FireBase](https://console.firebase.google.com/) et créer un nouveau projet (nom,etc..)

2. Lier un compte Google à FireBase et attendre que le projet soit crée

3. Aller dans la partie _développer_ puis _Hosting_

4. `[Sur WebStorm]` Se rendre sur un project avec diffèrents dossier (pour diffèrentes page)

5. Aller sur le _terminal_ et rentrer la commande `npm install -g firebase-tools`

> Checker la version avec  `firebase --version`

6. Intégrer des liens menant à d'autre dossier (faire un menu)

7. Se connécter via le terminal avec la commande `firebase login`

 Un naviguateur va se créer, rentrer les _identifiants_ du compte

> Un message devrait confirmer la connection du compte à firebase

9. Marquer dans la console `firebase init hosting`

10. Un message de [Confirmation](../pictures/confirmationFB.PNG) devrait apparaitre

11. Taper Y pour Yes 

12. Ouvrir un fichier déja existant puis le sélectioner 

13. Taper ./ puis `2 fois N pour No `

14. Plusieur fichiers devraients être crées.

15. Trouver `firebase.json`

16. Rentrer en dessous de `"**/node_modules/**"` :`"package.json","package-lock.json"`

> Cela évitera la publication et la diffusion d'information

>> Le fichier `.firebaserc` permet de bien indiquer le nom du projets

17. Toujours dans le terminal, taper `firebase deploy`

18. Ainsi, le Hosting URL devrait apparaitre et peut etre utiliser

19. Vérifier que les diffèrents fichiers soit bien actif

20. Pour ça, aller sur l'url, inspécter, puis Network => `all et ctrl + R`

21. Si il manque bootstrap = crée le dossier `vendors` et chercher le `dist`

22. Le copier et le coller dans le `vendors` et le rename `bootstrap`

23. Faire de meme pour le jquery = `dist, copier coler dans vendors et rename jquery`

24. Dans l'index retirer le node-module et rajouter le chemin d'accès à `bootstrap-grid.min.css`

25. `<link rel="stylesheet" href="../">`

26. Relancer `firebase deploy` et vérifier l'état des diffèrents fichiers ...





