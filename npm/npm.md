# Introduction à NPM

## Qu'est ce que NPM ?

**NPM pour _Node Package Manager_ est utiliser via le _terminal_ d'un éditeur de texte.**

**Il permet d'installer des plugins node.**

**TOUJOURS DANS LE TERMINAL**

## Quelque exemple et plugins importants

### bootstrap
***
`npm init`

`npm install` ou `npm i` + `bootstrap` **Attention à l'orthographe**

### git
***
`[A installer avant]` via [le site git](https://git-scm.com/)

`git init`


### jquery
***

`npm install jquery`  **Attention à l'orthographe**

### fireBase
***

`npm install -g firebase-tools`

`firebase --version` > pour check la version

`firebase login` 

`firebase init hosting`
