# Install Node

## A quoi sert-il ?

- **Node.js** sert à faire du Javascript server side, et peut être utilisé dans des applications de bases de données (BDD),
créer des applications en temps réel, 
où le serveur a la possibilité de transmettre de l’information au client.
 
- **Node.js a de nombreux avantages :** 

    Système de paquet intégré (NPM), modèle non bloquant, performance du moteur V8, logiciel libre (licence MIT). 
    Il dispose également d'une communauté très active. Son principal atout est la possibilité de coder en **Javascript.** 

- On utilise **Node.js** pour faire des applications cross-plateforme avec des frameworks 
comme **Ionic** pour les téléphones ou encore **Electron** pour des ordinateurs portables. 
Il est aussi employé parfois pour faire des serveurs web. 

## L'Installation

- ...

- ...

`[étapes annexes pas encore montrées !]`

- faire un `cd server` avant tout

- puis `npm i` pour tous les fichier

- Installer `npm install -g concurrently` dans le terminal
> Il permet de réaliser plusieur tache en même temps !

- Clique droit sur `package.json` puis `Show NPM Script`

- Lancer `Watch` au tout début et attendre la fin de la compilation

> Une fois lancer pour la première fois, il ne sera plus utile de le lancer.

- Lancer `serve`. **Le watcher et le server seront ouvert à chaque lancement**

- Désormais, il y aura 2 fichiers server :
    - `server.ts` qui sera un fichier typeScript, tout les modif seront envoyées au server.js automatiquement.
    
    - `server.js` qui sera un fichier javaScript, qui prendra les modif ajoutées dans server.ts.

## Code de base

- `[voici un code de base pour faire fonctioner un server]` 

```javascript
//1
import express from 'express';

//2
const app = express();

//3
app.get('/api', (req, res) => {
    return res.send('hello world');
});

//4
app.listen(3015, function () {
    console.log('API listening on http://localhost:3015/api/ !');
});
```

1. En premier lieu nous allons importer `express` qui est un outil de node pour créer un serveur.

2. Puis créer une variable `app` égale à `express`.

3. Ici, nous allons découvrir le premier verbe `get` qui permet d'obtenir une data.

    Donc lorsque l'on rentre l'url `/api` nous allons obtenir la requête avec `req` et envoyer une respond avec `res`.
    
    Ainsi, nous allons return `res.send` pour renvoyer la réponse ('hello world').
    
4. Et en dernière partie, `app.listen` qui définira le port et `console.log` le lien pour accéder au serveurs.

## Connecter l'appli Node à une base de données

1) S'assurer que firebase est bien installé (firebase --version)

2) Aller sur le site de Firebase et créer un nouveau projet

3) Désactiver google analytics

4) Aller sur Database

5) Cliquer sur "Créer une base de données"

6) Choisir "démarrer en mode de production"

7) Choisir europe3

8) Aller dans le terminal, s'assurer d'être dans server et saisir firebase init.

9) Choisir firestore

10) Use an existing project

11) Choisir le projet

12) Appuyer sur entrer lors de la question sur les rules

13) Saisir entrer encore une fois

14) De nouveaux fichiers sont apparus dans server (à ajouter sur git)

15) Sur firebase, créer une collection

16) Chemin : /hotels

17) l'Id doit être généré automatiquement

18) Remplir les champs : champs : name, type : string, valeur: hotel rose;

19) Créer une nouvelle collection rooms et faire pareil en ajoutant deux rooms

## Initialiser la base de données dans le projet

1) Aller sur firebase et accéder à la console
2) Accéder à la documentation
3) Aller dans get started for admin
4) Aller dans cloud firestore, puis get started
5) Copier npm install firebase-admin --save
6) Coller en s'assurant d'être dans server sinon saisir cd server dans le terminal
7) S'assurer que tout a été installé dans package.json

## Se connecter à firebase admin

1) Aller sur server.ts
2) Faire un import `import admin from 'firebase-admin'`;
3) Copier et coller le code pour initialiser en google cloud :
````
admin.initializeApp({
    credential: admin.credential.applicationDefault()
});
````
4) Créer la base de donnée et la référence pour avoir accès à la BDD (la BDD étant sur firebase)
````
const db = admin.firestore();

const ref = db.collection('hotels');
````
5) Maintenant qu'on a accès à la référence on va rajouter le mot clef `async` (quand on doit récuperer des données sur le server firebase) :
````
app.get('/api/hostels', async (req, res) => {
    try {
        const hotels = await ref.get();
        return res.send(hotels);
    } catch (e) {
return res.status(500).send({error: 'erreur serveur :' + e.message});
    }
});
````
On met await pour dire qu'on attends le résultat (pas partout) et async quand on a des choses à chercher dans une BDD externe
En cliquant sur ctrl + get on aura une documentation.

