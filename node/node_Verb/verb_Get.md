# Verbe GET 

**La méthode `GET` demande une représentation de la ressource spécifiée. Les requêtes `GET` doivent uniquement être utilisées afin de récupérer des données.**

````
export async function getAllHostels(): Promise<HotelModel[]> {
    const hotelsQuerySnap: QuerySnapshot = await refHotels.get();
    const hotels: HotelModel[] = [];
    hotelsQuerySnap.forEach(hotelSnap => hotels.push(hotelSnap.data() as HotelModel));
    return hotels;
}

app.get('/api/hostels', async (req, res) => {
    try {
        const hostels = await getAllHostels();
        return res.send(hostels);
    } catch (e) {
        return res.status(500).send({error: 'erreur serveur :' + e.message});
    }
});
````
