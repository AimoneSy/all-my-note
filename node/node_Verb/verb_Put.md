# Verbe PUT

La méthode `PUT` remplace toutes les représentations actuelles de la ressource visée par le contenu de la requête.

````
export async function putHostel(hostelId: string, newHostel: HotelModel): Promise<HotelModel> {
    if (!newHostel || !hostelId) {
        throw new Error(`hostel data and hostel id must be filled`);
    }
    const hostelToPutRef: DocumentReference = await testIfHostelExistsById(hostelId);
    await hostelToPutRef.set(newHostel);
    return newHostel;
}

app.put('/api/hostels/:id', async (req, res) => {
    try {
        const id = req.params.id;
        const hotel = await putHostel(id, req.body);
        return res.send(hotel);
    } catch (e) {
        return res.status(500).send({error: 'erreur serveur :' + e.message});
    }
});
````
