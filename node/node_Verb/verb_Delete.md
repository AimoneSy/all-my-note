# Verbe Delete

La méthode `DELETE` supprime la ressource indiquée.

````
export async function deleteHotelById(hostelId: string) {
    if (!hostelId) {
        throw new Error('hotel id is missing');
    }
    const hotelref: DocumentReference = refHotels.doc(hostelId);
    const snapHostelToDelete: DocumentSnapshot = await hotelref.get();
    const hostelToDelete: HotelModel | undefined = snapHostelToDelete.data() as HotelModel;
    if (!hostelToDelete) {
     throw  new Error(`${hostelId} does not exists`);
    }
    await refHotels.doc(hostelId).delete();
    return `${hostelId} => delete ok`
}

app.delete('/api/hostels/:id', async (req, res) => {
    try {
        const id = req.params.id;
        const writeResult: WriteResult = await deleteHotelById(id);
        return res.send(writeResult);
    } catch (e) {
        return res.status(500).send({error: 'erreur serveur :' + e.message});
    }
});
````
