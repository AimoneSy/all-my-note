# Verbe POST

La méthode `POST` est utilisée pour envoyer une entité vers la ressource indiquée. Cela entraîne généralement un changement d'état ou des effets de bord sur le serveur.

````
export async function postNewHostel(newHostel: HotelModel): Promise<HotelModel> {
    if (!newHostel) {
        throw new Error(`new hostel must be filled`);
    }
    const addResult: DocumentReference<DocumentData> = await refHotels.add(newHostel);
    return {...newHostel, id: addResult.id};
}

app.post('/api/hostels', async (req, res) => {
    try {
        const newHostel = req.body;
        const addResult = await postNewHostel(newHostel);
        return res.send(addResult);
    } catch (e) {
        return res.status(500).send({error: 'erreur serveur :' + e.message});
    }
});
````
