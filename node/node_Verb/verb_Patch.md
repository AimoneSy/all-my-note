# Verbe PATCH

La méthode `PATCH` est utilisée pour appliquer des modifications partielles à une ressource.

````
export async function updateHostel(hostelId: string, newHostel: HotelModel): Promise<HotelModel> {
    if (!newHostel || !hostelId) {
        throw new Error(`hostel data and hostel id must be filled`);
    }
    const hostelToPutRef: DocumentReference = await testIfHostelExistsById(hostelId);
    await hostelToPutRef.update(newHostel);
    return newHostel;
}

app.patch('/api/hostels/:id', async (req, res) => {
    try {
        const id = req.params.id;
        const hotel = await updateHostel(id, req.body);
        return res.send(hotel);
    } catch (e) {
        return res.status(500).send({error: 'erreur serveur :' + e.message});
    }
});
````
