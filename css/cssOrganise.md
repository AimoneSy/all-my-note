# Comment bien organiser le CSS

## Gestion du css au sein d'un projet :

**Gérer les CSS de manière logique et propre :**

- Créer un dossier dans assets ou tous les fichiers CSS seront à l'interieur

- Créer plusieurs fichiers CSS et les rename selon leurs rôles (main, menu, all-background ...)

> Plus il y a de petits fichiers, mieux c'est !

- Utiliser la commande `@import` dans le fichiers CSS de base puis indiquer leur URL

> Moin il y a de contenu dans le CSS de base, mieux c'est !

`@import url('../assets/stylesheet/main')`


