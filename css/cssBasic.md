# Les bases du CSS 

**Un fichier CSS fonctionne de la sorte :**

```
[NOM de la balise HTML] { 
    Unité et valeur pour la balise;
    color: black;
    etc ...  ;
    ...  ;
}
```

## _Le style des caractères_

Le font (=**Les Caractères** ) possède de nombreux _**"objets"**_ CSS.

- `font-family` est le plus important pour changer la police d'écriture sur un site  

- `font-size` permet de modifier la taille des caractères en pixel ou autre valeurs

- `font-weight` définira le poid des polices exemple `Normal` et **`Bolder`**


## _Les couleurs_

Les couleurs sont éxprimées en RGB **(RED GREEN BLUE)** sur 255 bits par couleur primaire.

- Pour du texte, l'objets `color` est la plus approprié

- Pour une div, ce sera `background-color` 


## _La mise en page_

Il est important de définir la `height (= Hauteur )` et la `width (= Largeur )` de chaque div

La valeur `100%` après height ou width permet prendre la taille de la page, soit de l'écran



## _Les borders_

Sur une div ou une image, des `borders` peuvent être rajouter

- Il suffit d'ajouter l'objet `borders: [Couleur] [Apparence] [Largeur] ;`

    Exemple :
    ```
     Test{
        border: black solid 10px
        border: yellow dashed 15px
    }
    ```
    **Pour + d'exemple ! [Clique ICI](https://www.w3schools.com/cssref/pr_border.asp)**
    
    
## _Les box shadows_

Objets qui a à peu près le même principe que les Borders.

Il permet de crée des **effets d'ombres** sur n'importe quel éléments (Texte, image, div)   

- Il est rajouter avec l'objet `box-shadow: [Couleur] [Taille] ;`

    **Plus d'info [ICI](https://www.w3schools.com/cssref/css3_pr_box-shadow.asp)**