# gitIgnore

## A quoi sert gitIgnore

gitIgnore permet à git de lui donner des fichiers et de ne pas les prendre en compte lors de repo.

Les fichiers passeront en **jaune** si ils sont bien ignorés.

[ ATTENTION ] si un fichier a été crée avant d'être mis dans le gitIgnore, 

il sera pris en compte normalement et ignorera gitignore 

`Créer des le debut du projet .gitIgnore `

**Rappel**
***
> Rouge = non pris en compte par Git et attend d'être add

> Vert = pris en compte par git et en attente 

> Bleu = Liés à gitLab et à été modifier

> Blanc = Correctement upload, aucune MAJ ou autre 

> Jaune = A été blocker via le fichier gitIgnore et n'est plus pris en compte par git

***

## Comment l'utiliser ?

- Il suffit de crée (au mieu des le debut) le fichier `.gitignore` à la racine

- Puis y rajouter des fichiers à l'interrieur

- Exemple `.node_module` / `.idea` / `.firebase` + autre si nécessaire