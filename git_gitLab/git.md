# Comment Installer GIT ?

## Etape

1. Se rendre sur [Git](https://git-scm.com/download/win) et l'installer

2. Lancer le luncher d'install et faire next next ... (cocher desktop icon pour avoir le logiciel sur bureau)

3. Installer gitLab sur webStorm dans setting/plugins et chercher `gitLab project` et installer

4. Mettre chaque info (https://gitlab.com etc) + token = clique sur page et rentrer le token.

5. fermer valider et relancer WS.

## 2eme étape 

- ouvrir le terminal et rentrer > `git init`
***
> Rouge = non pris en compte par Git et attend d'être add

> Vert = pris en compte par git et en attente 

> Bleu = Pris depuis gitLab ( ou autre ) et à été modifier

> Blanc = Correctement upload, aucune MAJ ou autre 

> Jaune = A été blocker via le fichier gitIgnore et n'est plus pris en compte par git

***
1. donc clique droit sur doc rouge git/Add pour ajouter a git.

2. puis fleche verte pour commit et bleu pour update.

>**décocher les 2 case (TODO et Perform)**

- {Optionnel} insérer du text dans commit (Anglais apprécié!)

- Puis push and commit

- si demander , insérer login etc ..

## Prendre un master de gitLab

- Aller sur le projet/groupe

> par [exemple](https://gitlab.com/NetConsultTeam/boostrap-grid) 

- cliquer sur clone, puis clone HTTPS

- Sur WebStorm, cliquer sur VCS situer sur la bar du haut puis `Get from Version Control`

- Puis insérer le lien copier et patienter la 'copie'

