# la syntaxe en PHP

## L'intégration du PHP

La balise PHP est constituer de la sorte : 
```
<?php
echo "Hello World!";
?>
```

> Toujours terminer une ligne avec un point virgule

## Les commentaires en PHP

- Pour une ligne :
```
// Commentaire

# Commentaire
```

- Pour plusieure lignes :
```
/*
Block de Commentaires
*/ je ne suis pas un commentaire
```

- A l'intérieur d'un code :
```
$x = 10 /* + 5 */ + 20;

echo $x = 30
```

## L'écriture des états

- Les noms d'états

```
ECHO "Text Sample 1<hr />";
echo "Text Sample 2<hr />";
ecHO "Text Sample 3<hr />";
```
> Ici tout les `Echo` fonctionnent


- Les noms de variables 

```
$car = "volvo";
echo "My Car is a " . $car . "<br>";
echo "My Car is a " . $CAR . "<br>";
echo "My Car is a " . $cAr . "<br>";
```
> Seul le premier echo fonctionne

## L'écriture des variables

- Commence par un `$` suivi du nom de la variable = `$test`

- Doit impérativement commencer par une lettre ou un `_` underscore = `$_test`

- Ne commence jamais par un nombre = ~~`$45test`~~

- Une variable peut contenir que des caractères alpha-numerique, des chiffres et un underscore (A-z, 0-9 et _)

- Les noms de variables prennent en compte les majuscule ($test1 et $TEST1 sont 2 variables diffèrentes)

