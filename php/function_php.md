# Les fonctions en PHP

## Les Variables Scope 

Il existe 2 types de Scope :

- La **Global Scope** qui est une variable déclaré en dehors de la fonction est donc accessible partout dans le programme.

- La **Local Scope** qui à l'inverse, est déclaré à l'intérieur d'une fonction mais accessible uniquement dans cette fonction.


Exemple d'un **Global Scope** : 

```
$x = 5;

function test1() {
    echo "<p>Valeur de x est : $x </p>";
}
test1 ();

echo "<p>Valeur de x est : $x </p><hr />";

```
> Ici, la fonction test1 va envoyer une erreur car elle n'a pas la valeur de x

Exemple d'un **Local Scope** : 

```
function test2() {
    $y = 5; //LOCAL SCOPE
    echo "<p>Valeur de y est : $y </p><hr />";
}

test2();
 
echo "<p>Valeur de y est : $y </p><hr />";
```
> Ici, la fonction test2 va envoyer la valeur de Y tandis qu'echo va renvoyer une erreur

## Le mot clé $GLOBALS

Ce mot clé permet à l'interieur d'une fonction de rendre global une variable en dehors de cette fonction

```
$x = 30;
$y = 20;


function test1() {
    global $x, $y;
    $x = $x + $y;
}

test1();
echo $y;
```
> Ici, la valeur de sortie Y sera égal à 50 sans message d'erreur

## Le mot clé static

```
function test1() {
    static $x = 10;
    echo $x;
    $x++;
}

test1();
echo "<hr />";
test1();
echo "<hr />";
test1();
echo "<hr />";
test1();
```
> Ici, le mot clé static actualise à chaque éxecution de la fonction test1 la valeur de x
