# Les Output PHP

Un output est un flux en écriture seule, qui permet d'écrire dans le mécanisme de buffer de sortie de la même façon que les fonctions print et echo.

## L'Output Echo

Echo possède de nombreux paramètres contrairement à Print.

Il exécute des déclarations légèrement plus vite que Print

## L'Output Print

Print ne possède aucun paramètre 
