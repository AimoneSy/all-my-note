# Introduction à PHP

## Qu'est ce que PHP ?

PHP, (pour Hypertext Preprocessor) est un langage de programmation libre créer en 1994.

Il permet de créer des pages Web dynamiques via serveur HTTP mais aussi localement.

PHP est le plus souvent couplé à un serveur Apache.

Il est multi-plateforme : autant sur Linux qu'avec Windows il permet aisément de reconduire le même code sur un environnement à peu près semblable.

Libre, gratuit, simple d'utilisation et d'installation.

## Son installation et première utilisation

1. Pour utiliser PHP, installer [WAMP ou MAMP](https://www.wampserver.com/) 

2. Lancer WAMP/MAMP

3. Sur la bar de tâche, à droite, un logo vert devrait apparaître. Cela indique que le serveur WAMP a bien été lancée.

4. Un nouveau fichier wamp va apparaître dans le disque local C:
> le fichier `www` sera le plus important, tous les sites seront stockés à l'intérieur

5. Crée un fichier pour chaque site a l'intérieur de `www`

6. Vérifier le fonctionnement de WAMP en tapant `localhost` dans la bar de naviguation sur internet (Une page WampServer devrait être chargée)

7. Crée un document en format php : [test.php]

8. Pour tester la liaison entre le document php et WAMP, rentrer ce code dans le document php :

```
<!DOCTYPE html>
<html>
<body>

<?php
echo "Hello World!";
?>

</body>
</html>
```

 9. Sauvegarder le documement php dans le fichier de l'étape 5 (celui crée a l'intérieur de www)

 10. Enfin, taper l'adresse `localhost/[nom du fichier]/[nom du document PHP] 
> Hello World! devrait apparaïtre

## 1 - La syntaxe en PHP

[Clique ICI](php/syntax_php.md)

## 2 - Les fonctions en PHP

[Clique ICI](php/function_php.md)

